package com.android.example.moneytracker;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomSheetFragment extends BottomSheetDialogFragment{
 public String typeMessage="";

    public static BottomSheetFragment getInstance() {
        return new BottomSheetFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        ((Button) view.findViewById(R.id.submit_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("button clicked tag","button in bottomsheet is clicked");

                NewWordActivity activity = (NewWordActivity) getActivity();
                // Invoke Main Activity's processDatePickerResult() method.

                activity.processTypePickerResult(typeMessage);

            }
        });
        final CheckBox checkboxvariable=(CheckBox)view.findViewById(R.id.food_ch);
        final CheckBox checkboxvariable2=(CheckBox)view.findViewById(R.id.house_ch);
        final CheckBox checkboxvariable3=(CheckBox)view.findViewById(R.id.travelling_ch);
        final CheckBox checkboxvariable4=(CheckBox)view.findViewById(R.id.medical_ch);
        final CheckBox checkboxvariable5=(CheckBox)view.findViewById(R.id.misc_ch);


        checkboxvariable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxvariable.isChecked()){

                    typeMessage=typeMessage.concat(" food ");
                }


                    }
                });
        checkboxvariable2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxvariable2.isChecked()){

                    typeMessage=typeMessage.concat(" house ");
                }

            }
        });
        checkboxvariable3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxvariable3.isChecked()){

                    typeMessage=typeMessage.concat(" travelling ");
                }

            }
        });
        checkboxvariable4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxvariable4.isChecked()){

                    typeMessage=typeMessage.concat(" medical ");
                }

            }
        });
        checkboxvariable5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxvariable5.isChecked()){

                    typeMessage=typeMessage.concat(" misc ");
                }

            }
        });

        return view;
    }






}
