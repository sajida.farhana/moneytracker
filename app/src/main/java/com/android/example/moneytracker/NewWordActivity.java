/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


// used for already filled data and passed for user to edit
import butterknife.OnClick;

import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_ID;
import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_UPDATE_amt;
import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_UPDATE_date;
import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_UPDATE_heading;
import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_UPDATE_notes;
import static com.android.example.moneytracker.MainActivity.EXTRA_DATA_UPDATE_type;

/**
 * This class displays a screen where the user enters a new word.
 * The NewWordActivity returns the entered word to the calling activity
 * (MainActivity), which then stores the new word and updates the list of
 * displayed words.
 */
public class NewWordActivity extends AppCompatActivity  {

    //Used to send new trans data
    public static final String EXTRA_REPLY_heading = "com.example.android.moneytracker.REPLY_heading";
    public static final String EXTRA_REPLY_type = "com.example.android.moneytracker.REPLY_type";
    public static final String EXTRA_REPLY_amt = "com.example.android.moneytracker.REPLY_amt";
    public static final String EXTRA_REPLY_date = "com.example.android.moneytracker.REPLY_date";
    public static final String EXTRA_REPLY_notes = "com.example.android.moneytracker.REPLY_notes";

    public static final String EXTRA_REPLY_ID = "com.android.example.moneytracker.REPLY_ID";

    private EditText mEditHeadingView;
    private EditText mEditTypeView;
    private EditText mEditAmtView;
    private EditText mEditDateView;
    private EditText mEditNotesView;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEditHeadingView = findViewById(R.id.editText_heading);
        int id = -1 ;
        mEditTypeView= findViewById(R.id.editText_type);
        mEditAmtView= findViewById(R.id.editText_amt);
        mEditDateView= findViewById(R.id.editText_date);
        mEditNotesView=findViewById(R.id.editText_notes);

        final Bundle extras = getIntent().getExtras();

        // If we are passed content, fill it in for the user to edit.
        if (extras != null) {
            String heading = extras.getString(EXTRA_DATA_UPDATE_heading, "");
            String type = extras.getString(EXTRA_DATA_UPDATE_type, "");
            int amt = extras.getInt(EXTRA_DATA_UPDATE_amt, 0);
            String date = extras.getString(EXTRA_DATA_UPDATE_date, "");
            String notes = extras.getString(EXTRA_DATA_UPDATE_notes, "");

            if (!heading.isEmpty()) {
                mEditHeadingView.setText(heading);
                //mEditTypeView.setSelection(type.length());
                mEditHeadingView.requestFocus();
            }
            if (!type.isEmpty()) {
                mEditTypeView.setText(type);
                //mEditTypeView.setSelection(type.length());

            }
            if (!(amt==0)) {
                mEditAmtView.setText(String.valueOf(amt));
                //mEditTypeView.setSelection(type.length());

            }
            if (!date.isEmpty()) {
                mEditDateView.setText(date);
                //mEditTypeView.setSelection(type.length());

            }
            if (!notes.isEmpty()) {
                mEditNotesView.setText(notes);
                //mEditTypeView.setSelection(type.length());

            }
        } // Otherwise, start with empty fields.


        final Button button = findViewById(R.id.button_save);

        // When the user presses the Save button, create a new Intent for the reply.
        // The reply Intent will be sent back to the calling activity (in this case, MainActivity).
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Create a new Intent for the reply.
                Intent replyIntent = new Intent();
                if ((TextUtils.isEmpty(mEditHeadingView.getText() ))||(TextUtils.isEmpty(mEditTypeView.getText() ))
                        ||(TextUtils.isEmpty(mEditAmtView.getText() ))
                ||(TextUtils.isEmpty(mEditDateView.getText() ))||(TextUtils.isEmpty(mEditNotesView.getText() )))
                {
                    // a trans was blank, set the result accordingly.
                    setResult(RESULT_CANCELED, replyIntent);

                } else {
                    // Get the new word that the user entered.
                    String heading = mEditHeadingView.getText().toString();
                    String type = mEditTypeView.getText().toString();
                    String amt = mEditAmtView.getText().toString();
                    int amt_num=Integer.parseInt(amt);
                    String date = mEditDateView.getText().toString();
                    String notes = mEditNotesView.getText().toString();

                    // Put the new word in the extras for the reply Intent.
                    replyIntent.putExtra(EXTRA_REPLY_heading, heading);
                    replyIntent.putExtra(EXTRA_REPLY_type, type);
                    replyIntent.putExtra(EXTRA_REPLY_amt, amt_num);
                    replyIntent.putExtra(EXTRA_REPLY_date, date);
                    replyIntent.putExtra(EXTRA_REPLY_notes, notes);

					if (extras != null && extras.containsKey(EXTRA_DATA_ID)) {
                        int id = extras.getInt(EXTRA_DATA_ID, -1);
                        if (id != -1) {
                            replyIntent.putExtra(EXTRA_REPLY_ID, id);
                        }
                    }
                    // Set the result status to indicate success.
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
    public void showDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(),
                getString(R.string.datepicker));
    }
    public void processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        String dateMessage = (month_string +
                "/" + day_string +
                "/" + year_string);
        mEditDateView.setText(dateMessage);


    }


    public void showTypePicker(View view) {
        BottomSheetFragment bottomSheetDialog = BottomSheetFragment.getInstance();
        bottomSheetDialog.show(getSupportFragmentManager(), getString(R.string.typepicker));
    }
    public void processTypePickerResult(String typeString)
    {
        String string_type=typeString;
        mEditTypeView.setText(string_type);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
