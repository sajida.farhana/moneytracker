/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Entity class that represents a word in the database
 */

@Entity(tableName = "trans_table")
public class Trans implements Serializable {


    @PrimaryKey(autoGenerate = true)
    public int trans_id;


    @ColumnInfo(name = "t_heading")
    private String mHeading;

    @ColumnInfo(name = "t_type")
    private String mType;

    @ColumnInfo(name = "t_amt")
    private int mAmt;



   // @TypeConverters({TimestampConverter.class})
    @ColumnInfo(name = "t_date")
    public String mDateData;

    @ColumnInfo(name = "notes")
    private String mNotes;




    public Trans(String heading, String type, int amt, String dateData, String notes) {
        this.mHeading = heading;
        this.mType = type;
        this.mAmt = amt;
        this.mDateData = dateData;
        this.mNotes = notes;


    }

    /*
    * This constructor is annotated using @Ignore, because Room expects only
    * one constructor by default in an entity class.
    */

    @Ignore
    public Trans(int id, String heading, String type, int amt, String dateData, String notes) {
        this.mHeading = heading;
        this.mType = type;
        this.mAmt = amt;
        this.mDateData = dateData;
        this.mNotes = notes;
        this.trans_id=id;
    }



    public String getHeading() {
        return this.mHeading;
    }
    public String getType() {
        return this.mType;
    }
    public int getAmt() {
        return this.mAmt;
    }
    public String getDate() {
        return this.mDateData;
    }
    public String getNotes() {
        return this.mNotes;
    }

    public void setHeading(String heading) {
        this.mHeading=heading;
    }
    public void setType(String type) {
        this.mType=type;
    }
    public void setAmt(int amt) {
        this.mAmt=amt;
    }
    public void setDate(String date) {
        this.mDateData=date;
    }
    public void setNotes(String notes) { this.mNotes=notes; }

    public int getId() {return trans_id;}

    public void setId(int id) {
        this.trans_id = id;
    }

}
