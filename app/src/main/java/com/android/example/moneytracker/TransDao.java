/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Data Access Object (DAO) for a word.
 * Each method performs a database operation, such as inserting or deleting a word,
 * running a DB query, or deleting all words.
 */

@Dao
public interface TransDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Trans trans);

    @Query("DELETE FROM trans_table")
    void deleteAll();

    @Delete
    void deleteTrans(Trans trans);

    @Query("SELECT * from trans_table LIMIT 1")
    Trans[] getAnyTrans();

    @Query("SELECT * from trans_table ORDER BY trans_id ASC")
    LiveData<List<Trans>> getAllTrans_id_sorted();

    @Query("SELECT * from trans_table ORDER BY t_amt ASC")
    LiveData<List<Trans>> getAllTrans_amt_sorted();

    //@Query("SELECT * from trans_table WHERE trans_id=:mtrans_id")
    //LiveData<Trans> getTrans(int mtrans_id);


    //insert a similar query based on sorting date
    @Query("SELECT * FROM trans_table ORDER BY t_date DESC")
    LiveData<List<Trans>> getAllTrans_date_sorted();

    @Query("SELECT * from trans_table WHERE t_heading LIKE '%' || :searchString || '%'")
    LiveData<List<Trans>> getTrans_searched(String searchString);

    @Update
    void update(Trans... trans);
}
