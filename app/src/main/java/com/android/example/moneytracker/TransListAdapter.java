/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Adapter for the RecyclerView that displays a list of transactions.
 */

public class TransListAdapter extends RecyclerView.Adapter<TransListAdapter.TransViewHolder> {

    private final LayoutInflater mInflater;
    private List<Trans> mTrans; // Cached copy of trans objects
	private static ClickListener clickListener;

    TransListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TransViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new TransViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TransViewHolder holder, int position) {
        if (mTrans != null) {
            Trans current = mTrans.get(position);
            holder.headingItemView.setText(current.getHeading());
           // holder.idItemView.setText(current.getId());
            holder.typeItemView.setText(current.getType());
            holder.amtItemView.setText(String.valueOf(current.getAmt()));
            holder.dateItemView.setText(current.getDate());
            holder.notesItemView.setText(current.getNotes());

        } else {
            // Covers the case of data not being ready yet.
           // holder.idItemView.setText("no id");
            holder.headingItemView.setText("no heading");
            holder.typeItemView.setText("no type");
            holder.amtItemView.setText("no amt");
            holder.dateItemView.setText("no date");
            holder.notesItemView.setText("no notes");
        }
    }

    /**
     * Associates a list of trans with this adapter
    */
    void setTrans(List<Trans> trans) {
        mTrans = trans;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * mTrans has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (mTrans != null)
            return mTrans.size();
        else return 0;
    }

    /**
     * Gets the trans at a given position.
     * This method is useful for identifying which trans
     * was clicked or swiped in methods that handle user events.
     *
     * @param position The position of the trans in the RecyclerView
     * @return The trans at the given position
     */
    public Trans getTransAtPosition(int position) {
        return mTrans.get(position);
    }

    class TransViewHolder extends RecyclerView.ViewHolder {
        private final TextView headingItemView;
        private final TextView typeItemView;
        //private final TextView idItemView;
        private final TextView amtItemView;
        private final TextView dateItemView;
        private final TextView notesItemView;



        private TransViewHolder(View itemView) {
            super(itemView);
            //idItemView = itemView.findViewById(R.id.textView_id);
            headingItemView=itemView.findViewById(R.id.textView_heading);
            typeItemView=itemView.findViewById(R.id.textView_type);
            amtItemView=itemView.findViewById(R.id.textView_amt);
            dateItemView=itemView.findViewById(R.id.textView_date);
            notesItemView=itemView.findViewById(R.id.textView_notes);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        TransListAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
