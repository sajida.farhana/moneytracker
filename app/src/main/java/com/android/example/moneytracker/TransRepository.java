/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

/**
 * This class holds the implementation code for the methods that interact with the database.
 * Using a repository allows us to group the implementation methods together,
 * and allows the TransViewModel to be a clean interface between the rest of the app
 * and the database.
 *
 * For insert, update and delete, and longer-running queries,
 * you must run the database interaction methods in the background.
 *
 * Typically, all you need to do to implement a database method
 * is to call it on the data access object (DAO), in the background if applicable.
 */

public class TransRepository {

    private TransDao mTransDao;
    private LiveData<List<Trans>> mAllTrans;
    private LiveData<List<Trans>> mAllTrans_amt;
    //private LiveData<List<Trans>> mAllTrans_searched;

    TransRepository(Application application) {
        TransRoomDatabase db = TransRoomDatabase.getDatabase(application);
        mTransDao = db.transDao();
        mAllTrans = mTransDao.getAllTrans_id_sorted();
        mAllTrans_amt=mTransDao.getAllTrans_amt_sorted();


    }

    LiveData<List<Trans>> getAllTrans_id_sorted() {
        return mAllTrans;
    }
    // do similar for amt sorted

    LiveData<List<Trans>> getAllTrans_amt_sorted() {
        return mAllTrans_amt;
    }

    public LiveData<List<Trans>>getTransSearched( String query)
    //removed Context context above
    {
        return mTransDao.getTrans_searched(query);
    }



    public void insert(Trans trans) {
        new insertAsyncTask(mTransDao).execute(trans);
    }

    public void update(Trans trans)  {
        new updateTransAsyncTask(mTransDao).execute(trans);
    }

    public void deleteAll()  {
        new deleteAllTransAsyncTask(mTransDao).execute();
    }

    // Must run off main thread
    public void deleteTrans(Trans trans) {
        new deleteTransAsyncTask(mTransDao).execute(trans);
    }

    // Static inner classes below here to run database interactions in the background.

    /**
     * Inserts a trans into the database.
     */
    private static class insertAsyncTask extends AsyncTask<Trans, Void, Void> {

        private TransDao mAsyncTaskDao;

        insertAsyncTask(TransDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trans... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    /**
     * Deletes all trans from the database (does not delete the table).
     */
    private static class deleteAllTransAsyncTask extends AsyncTask<Void, Void, Void> {
        private TransDao mAsyncTaskDao;

        deleteAllTransAsyncTask(TransDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    /**
     *  Deletes a single trans from the database.
     */
    private static class deleteTransAsyncTask extends AsyncTask<Trans, Void, Void> {
        private TransDao mAsyncTaskDao;

        deleteTransAsyncTask(TransDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trans... params) {
            mAsyncTaskDao.deleteTrans(params[0]);
            return null;
        }
    }

    /**
     *  Updates a trans in the database.
     */
    private static class updateTransAsyncTask extends AsyncTask<Trans, Void, Void> {
        private TransDao mAsyncTaskDao;

        updateTransAsyncTask(TransDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trans... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    /**
     *  searches a trans in the database.

    private static class searchTransAsyncTask extends AsyncTask<Trans, Void, Void> {
        private TransDao mAsyncTaskDao;

        searchTransAsyncTask(TransDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trans... params) {
            mAsyncTaskDao.getTrans(params[0]);
            return null;
        }
    }
        */



}
