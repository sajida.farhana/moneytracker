/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.sql.Date;

/**
 * TransRoomDatabase. Includes code to create the database.
 * After the app creates the database, all further interactions
 * with it happen through the TransViewModel.
 */

@Database(entities = {Trans.class}, version = 3, exportSchema = false)
public abstract class TransRoomDatabase extends RoomDatabase {

    public abstract TransDao transDao();

    private static TransRoomDatabase INSTANCE;

    public static TransRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TransRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here.
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TransRoomDatabase.class, "trans_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    // This callback is called when the database has opened.
    // In this case, use PopulateDbAsync to populate the database
    // with the initial data set if the database has no entries.
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    // Populate the database with the initial data set
    // only if the database has no entries.
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final TransDao mDao;

        // Initial data set
        private static String heading = "First Heading";
        private static String type = "First type";
        private static int amt = 0;
        private static String dateData = "null";
        private static String notes = "null";


        PopulateDbAsync(TransRoomDatabase db) {
            mDao = db.transDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // If we have no words, then create the initial list of words.
            if (mDao.getAnyTrans().length < 1) {
                Trans trans = new Trans(heading, type, amt, dateData, notes);
                mDao.insert(trans);
            }

            return null;
        }
    }

}

