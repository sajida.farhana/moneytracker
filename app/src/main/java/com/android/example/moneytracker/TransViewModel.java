/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.moneytracker;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

/**
 * The TransViewModel provides the interface between the UI and the data layer of the app,
 * represented by the Repository
 */

public class TransViewModel extends AndroidViewModel {

    private TransRepository mRepository;

    private LiveData<List<Trans>> mAllTrans;
    private LiveData<List<Trans>> mAllTrans_amt;

    public TransViewModel(Application application) {
        super(application);
        mRepository = new TransRepository(application);
        mAllTrans = mRepository.getAllTrans_id_sorted();
        mAllTrans_amt=mRepository.getAllTrans_amt_sorted();
    }

    LiveData<List<Trans>> getAllTrans_id_sorted() {
        return mAllTrans;
    }

    LiveData<List<Trans>> getAllTrans_amt_sorted() {
        return mAllTrans_amt;
    }

    public LiveData<List<Trans>>getTransSearched( String query)
    //removed Context context above
    {
        return mRepository.getTransSearched(query);
    }

    public void insert(Trans trans) {
        mRepository.insert(trans);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    public void deleteTrans(Trans trans) {
        mRepository.deleteTrans(trans);
    }

    public void update(Trans trans) {
        mRepository.update(trans);
    }

    //yet to add more
}
